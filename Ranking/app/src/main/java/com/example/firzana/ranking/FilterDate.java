package com.example.firzana.ranking;

import android.app.FragmentTransaction;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FilterDate extends AppCompatActivity implements View.OnClickListener {
    Button btnAll,btnFilter;
    EditText txtstart,txtend;
    ListView listView;

    // Server Http URL "http://firzanastorage-001-site1.1tempurl.com/filter.php?start=2017-03-02&end=2017-03-06"

    // String to hold complete JSON response object.
    String FinalJSonObject ;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_date);

        txtstart=(EditText) findViewById(R.id.txtstart);
        txtend=(EditText) findViewById(R.id.txtend);
        btnAll=(Button)findViewById(R.id.btnAll);
        btnAll.setOnClickListener(this);
        btnFilter=(Button)findViewById(R.id.btnFilter);
        btnFilter.setOnClickListener(this);
        progressBar = (ProgressBar)findViewById(R.id.progress);
        listView=(ListView)findViewById(R.id.lv_data);

        btnAll.callOnClick();

    }

    public void onStart(){
        super.onStart();
        EditText txtstart=(EditText)findViewById(R.id.txtstart);
        EditText txtend=(EditText)findViewById(R.id.txtend);
        txtstart.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    DateDialog dialog=new DateDialog(v);
                    FragmentTransaction ft=getFragmentManager().beginTransaction();
                    dialog.show(ft,"DatePicker");
                }
            }
        });
        txtend.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    DateDialog dialog=new DateDialog(v);
                    FragmentTransaction ft=getFragmentManager().beginTransaction();
                    dialog.show(ft,"DatePicker");
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        String startdate=txtstart.getText().toString();
        String enddate=txtend.getText().toString();

        switch (v.getId()){
            case R.id.btnAll:
                    try {
                        String HTTP_URL = "http://firzanastorage-001-site1.1tempurl.com/all_data.php";
                        //code
                        //Showing progress bar just after button click.
                        progressBar.setVisibility(View.VISIBLE);

                        // Creating StringRequest and set the JSON server URL in here.
                        StringRequest stringRequest = new StringRequest(HTTP_URL,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {

                                        // After done Loading store JSON response in FinalJSonObject string variable.
                                        FinalJSonObject = response;
                                        // Calling method to parse JSON object.
                                        new ParseJSonDataClass(FilterDate.this).execute();
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        // Showing error message if something goes wrong.
                                        Toast.makeText(FilterDate.this, error.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                });
                        RequestQueue requestQueue = Volley.newRequestQueue(FilterDate.this);
                        requestQueue.add(stringRequest);
                    }catch (Exception e){
                        Toast.makeText(FilterDate.this, "Couldn't get information. Please check your network connection or select dates", Toast.LENGTH_LONG).show();
                    }
                break;

            case R.id.btnFilter:
                if (!TextUtils.isEmpty(startdate)||!TextUtils.isEmpty(enddate)){
                    try {
                        String HTTP_URL_FILTER = "http://firzanastorage-001-site1.1tempurl.com/filter.php?start=" + startdate + "&end=" + enddate;
                        progressBar.setVisibility(View.VISIBLE);

                        StringRequest stringRequestFilter = new StringRequest(HTTP_URL_FILTER,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {

                                        // After done Loading store JSON response in FinalJSonObject string variable.
                                        FinalJSonObject = response;
                                        // Calling method to parse JSON object.
                                        new ParseJSonDataClass(FilterDate.this).execute();
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        // Showing error message if something goes wrong.
                                        Toast.makeText(FilterDate.this, error.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                });
                        RequestQueue requestQueueFilter = Volley.newRequestQueue(FilterDate.this);
                        requestQueueFilter.add(stringRequestFilter);
                    }catch (Exception e){
                        Toast.makeText(FilterDate.this, "No information on this date. Please select other dates", Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(FilterDate.this, "Please insert start and end dates", Toast.LENGTH_LONG).show();
                }

                break;
        }
    }

    private class ParseJSonDataClass extends AsyncTask<Void, Void, Void>{
        public Context context;
        // Creating List of Website class.
        List<Website> customWebsiteNamesList;

        public ParseJSonDataClass(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                // Checking whether FinalJSonObject(json response) is not equals to null.
                if (FinalJSonObject != null) {
                    // Creating and setting up JSON array as null.
                    JSONArray jsonArray = null;
                    try {

                        // Adding JSON response object into JSON array.
                        jsonArray = new JSONArray(FinalJSonObject);
                        // Creating JSON Object.
                        JSONObject jsonObject;
                        // Creating Website class object.
                        Website website;
                        // Defining customWebsiteNamesList AS Array List.
                        customWebsiteNamesList = new ArrayList<Website>();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            website = new Website();
                            jsonObject = jsonArray.getJSONObject(i);
                            //Storing ID,name,date,totalvisit into website list.
                            //website.Website_ID = jsonObject.getString("id_website");
                            website.Website_Name = jsonObject.getString("website_name");
                            website.Website_VisitDate = jsonObject.getString("visit_date").trim();
                            website.Website_TotalVisits = jsonObject.getString("total_visits").trim();

                            customWebsiteNamesList.add(website);
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (FinalJSonObject.equals("No Results Found.")){
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(FilterDate.this);
                alertDialogBuilder.setMessage("No information found. Please choose other date");
                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                progressBar.setVisibility(View.GONE);
            }else {
                ListViewAdapter adapter = new ListViewAdapter(customWebsiteNamesList, context);
                Toast.makeText(getApplicationContext(), "Top " + adapter.getCount() + " Websites", Toast.LENGTH_SHORT).show();
                listView.setAdapter(adapter);
                progressBar.setVisibility(View.GONE);
            }
        }
    }
}
